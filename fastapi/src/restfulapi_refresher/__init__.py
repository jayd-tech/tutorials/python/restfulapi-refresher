import logging.config

from restfulapi_refresher.utils import get_config_folder_path

logging_conf_path = f"{get_config_folder_path()}/logging.conf"

logging.config.fileConfig(logging_conf_path)
