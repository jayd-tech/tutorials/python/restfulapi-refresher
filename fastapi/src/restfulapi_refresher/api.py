from logging import getLogger
from typing import Any, Dict
from marshmallow_sqlalchemy import SQLAlchemySchema
from sqlalchemy.sql.expression import func, select

from fastapi import FastAPI

from restfulapi_refresher.db import Cafe, get_session
from restfulapi_refresher.serializers import CafeSchema

LOGGER = getLogger(__name__)
app = FastAPI()


@app.get("/random")
def get_random_cafe() -> Dict[str, Any]:
    with (session := get_session()()).begin():
        cafe = session.execute(select(Cafe).order_by(func.random())).scalars().first()
        schema = CafeSchema()
        data = schema.dump(cafe)
        LOGGER.debug(f"CAFE is {data}")
        return data
