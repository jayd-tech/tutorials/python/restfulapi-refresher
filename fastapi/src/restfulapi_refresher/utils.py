import os
import os.path

PROJECT_NAME = "taxer"


def get_config_folder_path():
    return os.environ.get(
        "CONFIG_FOLDER_PATH",
        f'{os.path.expanduser("~")}/.{os.environ.get("PROJECT_NAME", "restfulapi-refresher")}/config',
    )
