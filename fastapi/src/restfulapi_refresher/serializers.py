from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

from restfulapi_refresher.db import get_session, Cafe


Session = get_session()


class CafeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Cafe
        load_instance = True
        sqla_session = Session
