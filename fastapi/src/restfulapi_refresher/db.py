from pathlib import Path
from sqlalchemy import Column, create_engine
from sqlalchemy import Boolean, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker

BASE = declarative_base()


def get_db_file_path():
    return Path(Path(__name__).parent.parent, "cafes.db")


def get_engine():
    return create_engine(
        f"sqlite+pysqlite:///{get_db_file_path()}", echo=False, future=True
    )


def get_session():
    return sessionmaker(get_engine())


##Cafe TABLE Configuration
class Cafe(BASE):
    __tablename__ = "cafe"
    id = Column(Integer, primary_key=True)
    name = Column(String(250), unique=True, nullable=False)
    map_url = Column(String(500), nullable=False)
    img_url = Column(String(500), nullable=False)
    location = Column(String(250), nullable=False)
    seats = Column(String(250), nullable=False)
    has_toilet = Column(Boolean, nullable=False)
    has_wifi = Column(Boolean, nullable=False)
    has_sockets = Column(Boolean, nullable=False)
    can_take_calls = Column(Boolean, nullable=False)
    coffee_price = Column(String(250), nullable=True)
