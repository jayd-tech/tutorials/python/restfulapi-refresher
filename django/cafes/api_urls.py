from django.urls import path

from . import views

app_name = "cafes_api"
urlpatterns = [
    path("random", views.get_random_cafe, name="random"),
]
