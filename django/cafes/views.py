from typing import Dict, Union
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.template import loader
from rest_framework.decorators import api_view
from rest_framework.response import Response


from .models import Cafe
from .serializers import CafeSerializer

# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the cafes index.")


@api_view(["GET"])
def get_random_cafe(request) -> Dict[str, Union[str, bool]]:
    """
    Randomly select a Cafe instance and return its details.
    """
    cafe = Cafe.objects.order_by("?").first()
    if not cafe:
        pass  # API 404?
    serializer = CafeSerializer(cafe)
    return Response(serializer.data)


def detail(request, cafe_id) -> Dict[str, Union[str, bool]]:
    cafe_info = Cafe.objects.filter(id=cafe_id).values().first()
    if not cafe_info:
        raise Http404
    del cafe_info["id"]
    return render(
        request,
        "cafes/detail.html",
        {"name": cafe_info.pop("name"), "info": cafe_info},
    )
