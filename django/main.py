from logging import getLogger
from restfulapi_refresher.db import get_base

BASE = get_base()
LOGGER = getLogger(__name__)


## HTTP GET - Read Record

## HTTP POST - Create Record

## HTTP PUT/PATCH - Update Record

## HTTP DELETE - Delete Record
