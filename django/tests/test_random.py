from typing import Tuple, Generator
from fastapi.testclient import TestClient
import pytest
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import select

from restfulapi_refresher.api import app
from restfulapi_refresher.db import Cafe, get_session


@pytest.fixture
def test_client() -> TestClient:
    return TestClient(app)


@pytest.fixture
def session() -> Session:
    yield (session := get_session()())
    session.rollback()
    session.close()


@pytest.fixture
def five_sessions() -> Generator[Session, None, None]:
    sessionmaker = get_session()
    sessions = [sessionmaker() for _ in range(5)]
    for session in sessions:
        yield session
    for session in sessions:
        session.rollback()
        session.close()


@pytest.fixture()
def all_cafes() -> Tuple[Cafe]:
    session = get_session()()
    with session.begin():
        cafes = session.execute(select(Cafe)).scalars()
        yield tuple(cafes)
    session.close()


def test_random_cafes(all_cafes: Tuple[Cafe], test_client: TestClient):
    """
    Querying
    """
    cafe_names = list(map(lambda cafe: cafe.name, all_cafes))
    for _ in range(10):
        response = test_client.get("/random")
        assert response.status_code == 200
        print(response.json())
        assert response.json()["name"] in cafe_names
